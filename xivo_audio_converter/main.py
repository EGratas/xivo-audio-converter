#!/usr/bin/python3
# -*- coding: utf-8 -*-
import configparser
import logging
import os.path
from logging.handlers import TimedRotatingFileHandler
from xivo_audio_converter.converter import Converter

logger = logging.getLogger(__name__)
converter = None

def load_config(file):
    config = configparser.ConfigParser()
    config.read(file)
    return {
        'src_dir': config.get('general', 'src_dir'),
        'dst_dir': config.get('general', 'dst_dir'),
        'compressor' : config.get('general', 'compressor'),
        'compressor_params' : config.get('general', 'compressor_params'),
        'dst_file_extension' : config.get('general', 'dst_file_extension')
    }


def setup_logging(log_file):
    root_logger = logging.getLogger()
    formatter = logging.Formatter('%(asctime)s (%(levelname)s) (%(name)s): %(message)s')
    handler = TimedRotatingFileHandler(filename=log_file, backupCount=15, when='midnight', interval=1)
    handler.setFormatter(formatter)
    root_logger.addHandler(handler)
    root_logger.setLevel(logging.INFO)


def validate_config(config):
    check_is_directory(config['src_dir'])
    check_is_directory(config['dst_dir'])


def check_is_directory(dir):
    if not os.path.isdir(dir):
        logger.error('%s is not a directory' % dir)
        exit(1)


def main():
    global converter
    config = load_config(os.getenv('CONFIG_FILE'))
    converter = Converter(config)
    setup_logging('/var/log/xivo-audio-converter/xivo-audio-converter.log')
    logger.info('Start Logging')
    validate_config(config)
    logger.info('Starting with configuration %s' % config)
    converter.run()
